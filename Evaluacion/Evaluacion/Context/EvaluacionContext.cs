﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Evaluacion.Context
{
    public class EvaluacionContext :DbContext
    {
        public EvaluacionContext(): base("DefaultConnection")
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public System.Data.Entity.DbSet<Evaluacion.Models.Headquartes> Headquartes { get; set; }

        public System.Data.Entity.DbSet<Evaluacion.Models.Teacher> Teachers { get; set; }

        public System.Data.Entity.DbSet<Evaluacion.Models.Speciality> Specialities { get; set; }
    }
}