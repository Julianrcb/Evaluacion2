﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Evaluacion.Context;
using Evaluacion.Models;

namespace Evaluacion.Controllers
{
    public class HeadquartesController : Controller
    {
        private EvaluacionContext db = new EvaluacionContext();

        // GET: Headquartes
        public ActionResult Index()
        {
            return View(db.Headquartes.ToList());
        }

        // GET: Headquartes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headquartes headquartes = db.Headquartes.Find(id);
            if (headquartes == null)
            {
                return HttpNotFound();
            }
            return View(headquartes);
        }

        // GET: Headquartes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Headquartes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idHeadquartes,nameHeadquartes")] Headquartes headquartes)
        {
            if (ModelState.IsValid)
            {
                db.Headquartes.Add(headquartes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(headquartes);
        }

        // GET: Headquartes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headquartes headquartes = db.Headquartes.Find(id);
            if (headquartes == null)
            {
                return HttpNotFound();
            }
            return View(headquartes);
        }

        // POST: Headquartes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idHeadquartes,nameHeadquartes")] Headquartes headquartes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(headquartes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(headquartes);
        }

        // GET: Headquartes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headquartes headquartes = db.Headquartes.Find(id);
            if (headquartes == null)
            {
                return HttpNotFound();
            }
            return View(headquartes);
        }

        // POST: Headquartes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Headquartes headquartes = db.Headquartes.Find(id);
            db.Headquartes.Remove(headquartes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
