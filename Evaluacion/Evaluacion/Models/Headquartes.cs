﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion.Models
{
    public class Headquartes
    {
        [Key]
        public int idHeadquartes { get; set; }

        [Required(ErrorMessage ="Este campo es obligatorio")]
        [Display(Name ="Nombre de la sede")]
        [StringLength(100)]
        public String nameHeadquartes  { get; set; }

        public virtual ICollection <Teacher> teacher { get; set; }
    }
}