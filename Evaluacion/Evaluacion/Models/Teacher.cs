﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion.Models
{
    public class Teacher
    {
        [Key]
        public int idTeacher { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Primer Nombre")]
        [StringLength(50)]
        public String firstName { get; set; }

        [Display(Name = "Segundo Nombre")]
        [StringLength(50)]
        public String secondName { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Primer Apellido")]
        [StringLength(100)]
        public String surname { get; set; }

        [Display(Name = "segundo Apellido")]
        [StringLength(100)]
        public String secondSurname { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Correo")]
        [StringLength(100)]
        [RegularExpression("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(gmail.com|zoho.com|outlook.com)$", ErrorMessage = "El correo debe ser: outlook, gmail o zoho.")]
        public String email { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Edad")]
        public byte years { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Fecha de nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime dateOfBirth { get; set; }

        public int idHeadquartes { get; set; }
        public int idSpeciality { get; set; }

        public virtual Speciality specialitys { get; set; }
        public virtual Headquartes headquartes { get; set; }
     }
}