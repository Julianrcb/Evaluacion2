﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion.Models
{
    public class Speciality
    {
        [Key]
        public int idSpeciality { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        [Display(Name = "Nombre de la especialidad")]
        [StringLength(100)]
        public String name { get; set; }

        public virtual ICollection<Teacher> teacher { get; set; }

    }
}